"""newStory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

import Story_6.urls as Story_6
import buku.urls as Story_9
import Story_10.urls as Story_10

app_name = ''
urlpatterns = [
    path('admin/', admin.site.urls),
	path('', RedirectView.as_view(url='story-6/profile')),
    path('story-6/', include(Story_6, namespace='Story_6')),
    path('story-9/', include(Story_9, namespace='Story_9')),
    path('story-10/', include(Story_10, namespace='Story_10')),
]
