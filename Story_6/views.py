from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .forms import StatusForm
from .models import Status

response = {}
def status_view(request):
    response['status'] = Status.objects.all()
    response['form'] = StatusForm()
    html = 'home.html'
    response['name'] = []
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    return render(request, html, response)


def add_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('Story_6:status'))
    else:
        form = StatusForm()
    return render(request, 'home.html', {'form': form,
                                         'status': Status.objects.all()})
def show_profile(request):
    response['name'] = []
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    return render(request, 'profile.html', response)

