from django import forms
from .models import Status


class StatusForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(StatusForm, self).__init__(*args, **kwargs)

        self.fields['status'].label = 'Status'
        self.fields['status'].widget = forms.Textarea(attrs={'class': 'form-control',
                                                             'placeholder': 'Apa yang sedang anda pikirkan?',
                                                             'rows': 5,
                                                             'cols': 8,
                                                             'style': 'resize : none'}
                                                      )
        self.fields['status'].error_messages = {'max_length': 'Batas maksimal karakter 300',
                                                'required': 'Status harus diisi'}

    class Meta:
        model = Status
        fields = ['status']
