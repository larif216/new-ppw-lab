from django.urls import path
from django.views.generic import RedirectView

from .views import status_view, add_status, show_profile

app_name = "Story_6"
urlpatterns = [
	path('', RedirectView.as_view(url='story-6/profile/')),
	path('status/', status_view, name="status"),
	path('add-status/', add_status, name='add_status'),
    path('profile/', show_profile, name='profile'),
]

