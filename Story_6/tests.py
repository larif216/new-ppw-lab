import datetime
from unittest import mock

import pytz
import time

from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.conf import settings
from importlib import import_module

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .forms import StatusForm
from .models import Status
from .views import status_view, show_profile


class Story6UnitTest(TestCase):

    def test_landing_page_exists(self):
        response = Client().get('/story-6/profile/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url_not_found(self):
        response = Client().get('/story-6/invalid-url/')
        self.assertEqual(response.status_code, 404)

    def test_root_url_redirects_to_profile(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/story-6/profile', 302, 301)

    def test_landing_page_using_show_profile_func(self):
        found = resolve('/story-6/profile/')
        self.assertEqual(found.func, show_profile)

    def test_landing_page_uses_template(self):
        response = Client().get('/story-6/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_landing_page_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = show_profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('This is My Profile', html_response)

    def test_model_can_create_new_status(self):
        Status.objects.create(
            status='Pusing gan',
            created_at=timezone.now()
        )
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)

    def test_model_date_auto_now_add_works(self):
        mocked_time = datetime.datetime(1999, 10, 23, 0, 0, 0, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_time)):
            new_status = Status.objects.create()
            self.assertEqual(new_status.created_at, mocked_time)

    def test_form_status_input_has_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())
        self.assertIn('rows="5"', form.as_p())
        self.assertIn('cols="8"', form.as_p())

    def test_form_validation_for_blank_status(self):
        form = StatusForm(data={
            'status': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'], ['Status harus diisi']
        )

    def test_form_validation_for_exceeding_status_length(self):
        form = StatusForm(data={
            'status': '300 karakter' * 1000,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'], ['Batas maksimal karakter 300']
        )

    def test_form_validation_for_maximum_status_length(self):
        form = StatusForm(data={
            'status': 'a' * 300,
        })
        self.assertTrue(form.is_valid())

    def test_get_form_function(self):
        response = Client().get('/story-6/add-status/')
        self.assertEqual(response.status_code, 200)

    def test_post_success_and_render_the_result(self):
        test_status = 'Pusing'
        response_post = Client().post('/story-6/add-status/',
                                      {
                                          'status': test_status
                                      })
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/story-6/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test_status, html_response)

    def test_post_error_and_render_the_result(self):
        test_status = 'Pusing'
        response_post = Client().post('/story-6/add-status/',
                                      {
                                          'status': ''
                                      })
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/story-6/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test_status, html_response)

    def test_post_success_and_exists_in_the_database(self):
        Client().post('/story-6/add-status/',
                      {
                          'status': 'Test masuk database'
                      })
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_post_error_and_does_not_exists_in_the_database(self):
        Client().post('/story-6/add-status/',
                      {
                          'status': ''
                      })
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 0)

    def test_profile_page_exists(self):
        response = Client().get('/story-6/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_show_profile_func(self):
        found = resolve('/story-6/profile/')
        self.assertEqual(found.func, show_profile)

    def test_profile_page_uses_template(self):
        response = Client().get('/story-6/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_page_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = show_profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('https://pbs.twimg.com/media/DmdGUTyU0AAp3dU.jpg', html_response)
        self.assertIn('Muhamad Lutfi Arif', html_response)

# class Story7FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver',
#                                          chrome_options=chrome_options)
#         super(Story7FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story7FunctionalTest, self).tearDown()
#
#     def test_input_status(self):
#         selenium = self.selenium
#         selenium.get('http://127.0.0.1:8000/story-6/')
#         time.sleep(2)
#         status = selenium.find_element_by_id('id_status')
#         submit_status = selenium.find_element_by_name('tombol_submit')
#         status.send_keys('Coba Coba')
#         time.sleep(2)
#         submit_status.click()
#         time.sleep(2)
#         posted_status = selenium.find_elements_by_id('Stats_content')[-1]
#         self.assertEquals("Coba Coba", posted_status.text)
#
#     def test_title_is_exist(self):
#         selenium = self.selenium
#         selenium.get('http://my-new-story.herokuapp.com/story-6/profile/')
#         title = selenium.find_element_by_id('title')
#         self.assertEquals(title.text, "This is My Profile")
#
#     def test_photo_profile_is_exist(self):
#         selenium = self.selenium
#         selenium.get('http://my-new-story.herokuapp.com/story-6/profile/')
#         photo = selenium.find_element_by_class_name('img-circle')
#         source = photo.get_attribute('src')
#         self.assertEquals('https://pbs.twimg.com/media/DmdGUTyU0AAp3dU.jpg', source)
#
#     def test_title_size_is_70px(self):
#         selenium = self.selenium
#         selenium.get('http://my-new-story.herokuapp.com/story-6/profile/')
#         title = selenium.find_element_by_id('title')
#         self.assertEquals(title.value_of_css_property('font-size'), '50px')
#
#     def test_photo_profile_has_border(self):
#         selenium = self.selenium
#         selenium.get('http://my-new-story.herokuapp.com/story-6/profile/')
#         photo = selenium.find_element_by_class_name('img-circle')
#         self.assertEquals(photo.value_of_css_property('border'), '10px solid rgb(33, 150, 243)')
#
#
#
#
#
#
#
#
#
#
#
