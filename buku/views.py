import json
import requests
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

BOOK_API = 'https://www.googleapis.com/books/v1/volumes?q='

response = {}


def list_buku(request):
    response['name'] = []
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    return render(request, 'buku.html', response)

def get_json(request, jenis):
    url = requests.get(BOOK_API + jenis)
    jsons = url.json()
    return JsonResponse(jsons)

@csrf_exempt
def token_verification(request):
    VERIFY_TOKEN_URL = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='
    token = request.POST['token']
    data = requests.get(VERIFY_TOKEN_URL + token).json()
    request.session['token'] = token
    request.session['name'] = data['name']
    request.session['image'] = data['picture']
    request.session['email'] = data['email']
    return HttpResponse(json.dumps({'log': 'Success'}))


def add_session_favorite(request, book_id):
    if 'books' not in request.session.keys():
        request.session['books'] = [book_id]
    else:
        books = request.session['books']
        if book_id not in books:
            books.append(book_id)
            request.session['books'] = books
    return HttpResponseRedirect(reverse('book-list:book-list'))


def delete_session_favorite(request, book_id):
    books = request.session['books']
    books.remove(book_id)
    request.session['books'] = books
    return HttpResponseRedirect(reverse('book-list:book-list'))


def clear_session(request):
    request.session.flush()
    return HttpResponse(json.dumps({'log': 'Success'}),
                        content_type='application/json')

