var counter = 0;
function favorit(id) {

    var objId = $('#' + id);
    var objImageSrc = objId[0].src;
    if (objImageSrc.includes('starOff')) {
        counter++;
        objId[0].src = starOn;
    } else {
        counter--;
        objId[0].src = starOff;
    }
    $('#fav').html('Jumlah buku favorit: ' + counter);
}