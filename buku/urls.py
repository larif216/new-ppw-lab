from django.urls import path
from django.views.generic import RedirectView

from .views import list_buku, get_json, token_verification, clear_session

app_name = "Story_9"
urlpatterns = [
    path('', list_buku, name='buku'),
    path('get-json/<str:jenis>', get_json, name='get-json'),
    path('verifikasi', token_verification, name='verifikasi'),
	path('clear-session', clear_session, name='clear-session'),
]

