from django.test import TestCase, Client
from django.urls import resolve


from .views import list_buku


class Story9UnitTests(TestCase):

    def test_book_list_url_is_exists(self):
        response = Client().get('/story-9/')
        self.assertEqual(200, response.status_code)

    def test_book_list_uses_correct_function(self):
        response = resolve('/story-9/')
        self.assertEqual(response.func, list_buku)

    def test_book_list_uses_correct_template(self):
        response = Client().get('/story-9/')
        self.assertTemplateUsed(response, 'buku.html')
