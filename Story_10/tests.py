from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .views import show_registration_form, validate_email, form_submission
from .models import Subscriber
from .forms import FormRegistrasi

import json

# Create your tests here.
class Story10UnitTest(TestCase):

    def setUp(self):
        Subscriber(nama='Muhamad Lutfi Arif',
                   email='test@gmail.com',
                   password='TestMasukDataBase').save()

    def test_url_is_exist(self):
        response = Client().get('/story-10/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist2(self):
        response = Client().get('/story-10/validasi-email/')
        self.assertEqual(response.status_code, 200)
        response = validate_email(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('false', html)

    def test_using_correct_func(self):
        response = resolve('/story-10/')
        self.assertEqual(response.func, show_registration_form)

    def test_using_correct_templates(self):
        response = Client().get('/story-10/')
        self.assertTemplateUsed(response, 'Story10.html')

    def test_can_create_objects_in_database(self):
        Subscriber.objects.create(nama='Muhamad Lutfi Arif',
                                  email='Larif216@gmail.com',
                                  password='TestMasukDataBase')
        jumlah = Subscriber.objects.all().count()
        self.assertEqual(jumlah, 2)

    def test_form_registrasi_has_css_classes(self):
        form = FormRegistrasi()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="nama"', form.as_p())
        self.assertIn('id="email"', form.as_p())
        self.assertIn('id="password"', form.as_p())

    def test_form_validation_for_exceeding_username_length(self):
        form = FormRegistrasi(data={
            'nama': 'Lutfi' * 30
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['nama'], ['Batas maksimal karakter 30'])

    def test_form_validation_for_blank_username(self):
        form = FormRegistrasi(data={
            'nama': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nama'], ['Nama tidak boleh kosong']
        )

    def test_form_validation_for_blank_email(self):
        nama = 'Lutfi'
        email = ''
        password = 'cobamasuk'
        response_post = Client().get('/story-10/submit-form/', {'nama': nama, 'email': email, 'password': password})
        self.assertEqual(response_post.status_code, 200)

    def test_form_validation_for_already_registered_email(self):
        response_post = Client().post('/story-10/submit-form/', {'nama': 'a', 'email': 'b@gmail.cpm', 'password': 'a'})
        response_post1 = Client().post('/story-10/submit-form/', {'nama': 'a', 'email': 'b@gmail.cpm', 'password': 'a'})
        json_data = json.loads(response_post1.content.decode('utf8'))
        self.assertEqual(json_data['message'], 'Registrasi gagal!')

    def test_form_successful_submission_response(self):
        nama = 'Upi'
        response = Client().post('/story-10/submit-form/', {
            'nama': nama,
            'email': 'test2@gmail.com',
            'password': 'snaaaaaaaaake!',
        })
        json_data = json.loads(response.content.decode('utf8'))
        self.assertTrue(json_data['success'])
        self.assertEqual(json_data['message'], 'Berhasil! Terimakasih, ' + nama + '. Anda terdaftar sebagai subscriber')


