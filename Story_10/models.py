from django.db import models

# Create your models here.
class Subscriber(models.Model):
    nama = models.CharField(max_length=30)
    email = models.EmailField(max_length=30, unique=True)
    password = models.CharField(max_length=30)