from django.shortcuts import render
from django.http import HttpResponse
from django.db import IntegrityError
import json

from .forms import FormRegistrasi
from .models import Subscriber
response = {}
# Create your views here.
def show_registration_form(request):
    form = FormRegistrasi(request.POST)
    response['name'] = []
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    response['form'] = form
    return render(request, 'Story10.html', response)

def show_subscriber(request):
    response['name'] = []
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    subscriber = Subscriber.objects.all()
    response['data'] = subscriber
    return render(request, 'list.html', response)

def get_subscriber(request):
    subscriber = Subscriber.objects.all()
    subscriber_list = [{'nama':obj.nama, 'email':obj.email} for obj in subscriber]
    return HttpResponse(json.dumps({'data':subscriber_list}))

def unsubscribe(request):
    if request.method == 'POST':
        subscriber_email = request.POST['subscriber_id']
        Subscriber.objects.get(email=subscriber_email).delete()
        return HttpResponse(json.dumps({'message': 'Berhasil unsubscribe!'}))
    else:
        return HttpResponse(json.dumps({'message': 'Gagal unsubscribe!'}))


def validate_email(request):
    response = {
        'message' : 'Email yang Anda gunakan valid',
        'is_taken' : False,
    }
    email = request.POST.get('email')
    response['is_taken'] = Subscriber.objects.filter(email=email).exists()
    if response['is_taken']:
        response['message'] = 'Email sudah terdaftar, silakan gunakan email yang lain!'
    json_data = json.dumps(response)
    return HttpResponse(json_data)

def form_submission(request):
    response = {}
    try:
        if request.method == 'POST':
            nama = request.POST.get('nama')
            email = request.POST.get('email')
            password = request.POST.get('password')
            Subscriber.objects.create(nama=nama, email=email, password=password)
            response['success'] = True
            response['message'] = 'Berhasil! Terimakasih, ' + nama + '. Anda terdaftar sebagai subscriber'
            json_data = json.dumps(response)
            return HttpResponse(json_data)
        else:
            response['success'] = False
            response['message'] = 'Registrasi gagal!'
            json_data = json.dumps(response)
            return HttpResponse(json_data)
    except IntegrityError as e:
        response['success'] = False
        response['message'] = 'Registrasi gagal!'
        json_data = json.dumps(response)
        return HttpResponse(json_data)
