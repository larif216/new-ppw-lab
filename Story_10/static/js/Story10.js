var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
var email_is_valid = false;

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function isValid(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function validasiInput() {
    if ($('#nama').val() && $('#email').val() && $('#password').val() && email_is_valid) {
        $('button[type=submit]').removeAttr('disabled');
    } else {
        $('button[type=submit]').attr('disabled', 'true');
    }
}

function unsubscribe(email) {
    var obj = document.getElementById(email)
    obj.remove()
    $.ajax({
        method: 'POST',
        url: 'unsubscribe/',
        type: 'json',
        data: {'subscriber_id': email},
    });
}

$(document).ready(function () {
    $('#email').after("<div id='invalid_email' style='display: none' class='alert alert-danger alert-dismissible fade show' role='alert'></div>");
    $('#email').after("<div id='valid_email' style='display: none' class='alert alert-success alert-dismissible fade show' role='alert'></div>");
    $('button[type=submit]').after("<div id='failed' style='display: none' class='alert alert-danger' role='alert'></div>");
    $('button[type=submit]').after("<div id='success' style='display: none' class='alert alert-success' role='alert'></div>");
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $.ajax({
        url: 'get-subscriber/',
        dataType: 'json',
        success: function (list_subscriber) {
            var i;
            for (i = 0; i < list_subscriber.data.length; i++) {
                var nama = list_subscriber.data[i].nama
                var email = list_subscriber.data[i].email
                var button = "<button id=" + email +" onclick=unsubscribe(id) type='submit' class='btn btn-danger unsubscribe' value='unsubscribe'>Unsubscribe</button>"

                $('#table-subscriber').append('<tr id=' + email + '><td>' + nama + '</td><td>' + email + '</td><td>' + button + '</td></tr>')
            }
        }
    });
    $(document).on('change', '#nama, #email, #password',function () {
        validasiInput();
    });

    $('#email').on('change', function () {
            var email = $(this).val();
            $.ajax({
                method: 'POST',
                url: 'validasi-email/',
                data: {
                    'email': email
                },
                dataType: 'json',
                success: function (result) {
                    var buttonClose = "<button type='button' " +
                                      "class='close' data-dismiss='alert' aria-label='Close'>" +
                                      "<span aria-hidden='true'>&times;</span></button>"
                    if (isValid(email)) {
                        if (result.is_taken) {
                            $('#invalid_email').html(result.message + buttonClose);
                            $('#invalid_email').css('display', 'block');
                            $('#valid_email').css('display', 'none');
                            $('#email').css('border-color', 'red');
                        } else {
                            $('#valid_email').html(result.message + buttonClose);
                            $('#valid_email').css('display', 'block');
                            $('#invalid_email').css('display', 'none');
                            $('#email').css('border-color', '#ced4da');
                            email_is_valid = !result.is_taken;
                        }
                    } else {
                        $('#invalid_email').html('Email tidak valid!' + buttonClose);
                        $('#invalid_email').css('display', 'block');
                        $('#valid_email').css('display', 'none');
                        $('#email').css('border-color', 'red');
                    }
                }
            })
        });

    $('#form-registrasi').on('submit', function () {
        var nama = $('#nama');
        var email = $('#email');
        var password = $('#password');
        var data = {
            'nama': nama.val(),
            'email': email.val(),
            'password': password.val(),
        };
        $.ajax({
            method: 'POST',
            url: 'submit-form/',
            dataType: 'json',
            data: data,
            success: function (result) {
                if (result.success) {
                    $('#success').html(result.message);
                    $('#success').css('display', 'block');
                    $('#failed').css('display', 'none');
                    $('#valid_email').css('display', 'none');
                    $('#invalid_email').css('display', 'none');
                    nama.val("");
                    email.val("");
                    password.val("");
                } else {
                    $('#failed').html(result.message);
                    $('#failed').css('display', 'block');
                    $('#success').css('display', 'none');
                    $('#valid_email').css('display', 'none');
                    $('#invalid_email').css('display', 'none');
                }
            }
        });
        event.preventDefault();
    });
});