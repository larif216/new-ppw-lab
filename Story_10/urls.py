from django.urls import path
from .views import show_registration_form, validate_email, form_submission, show_subscriber, get_subscriber, unsubscribe


app_name = 'Story_10'
urlpatterns = [
    path('', show_registration_form, name='form-registrasi'),
    path('validasi-email/', validate_email, name='validate-email'),
    path('submit-form/', form_submission, name='add-subscriber'),
    path('list-subscriber/', show_subscriber, name='list-subscriber'),
    path('list-subscriber/get-subscriber/', get_subscriber, name='get-subscriber'),
    path('list-subscriber/unsubscribe/', unsubscribe, name='unsubscribe'),
]