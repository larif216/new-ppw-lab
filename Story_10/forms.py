from django import forms

from .models import Subscriber


class FormRegistrasi(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FormRegistrasi, self).__init__(*args, **kwargs)

        self.fields['nama'].label = ''
        self.fields['nama'].widget.attrs = {'id': 'nama', 'class': 'form-control', 'placeholder': 'Nama Lengkap'}
        self.fields['nama'].error_messages = {'max_length': 'Batas maksimal karakter 30',
                                                'required': 'Nama tidak boleh kosong'}

        self.fields['email'].label = ''
        self.fields['email'].widget.attrs = {'id': 'email', 'class': 'form-control', 'placeholder': 'Email'}
        self.fields['email'].error_messages = {'max_length': 'Batas maksimal karakter 30',
                                                'required': 'Email tidak boleh kosong'}

        self.fields['password'].label = ''
        self.fields['password'].widget = forms.PasswordInput()
        self.fields['password'].widget.attrs = {'id': 'password', 'class': 'form-control', 'placeholder': 'Password'}
        self.fields['password'].error_messages = {'max_length': 'Batas maksimal karakter 30',
                                                'required': 'Password tidak boleh kosong'}


    class Meta:
        model = Subscriber
        fields = ['nama', 'email', 'password']

